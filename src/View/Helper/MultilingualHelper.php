<?php
namespace Multilingual\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Renderer\PhpRenderer;
use Locale;
use Multilingual\Service\MultilingualService;

class MultilingualHelper extends AbstractHelper
{
    protected $service = null;

    public function __construct(MultilingualService $service)
    {
        $this->service = $service;
    }

    public function __invoke()
    {
        return $this;
    }

    public function detectedLocale() : ?string
    {
        if ($this->service) {
            return $this->service->detectedLocale();
        }
        return null;
    }

    public function translationLocale() : ?string
    {
        if ($this->service) {
            $detected = $this->service->detectedLocale();
            return $this->service->translationLocale($detected);
        }
        return null;
    }

    public function defaultLocale() : ?string
    {
        if ($this->service) {
            return $this->service->defaultLocale();
        }
        return null;
    }

    public function supportedLocales() : array
    {
        if ($this->service) {
            return $this->service->supportedLocales();
        }
        return [];
    }

    public function prepareView(PhpRenderer $view) : void
    {
        $view->headLink()->appendStylesheet($view->assetUrl('css/multilingual-locale-selector.css', 'Multilingual'));
        $view->headScript()->appendFile($view->assetUrl('js/multilingual-locale-selector.js', 'Multilingual'));
    }

    public function localeSelector() : string
    {
        $supported = $this->supportedLocales();

        // Don't display selector if no choice
        if (count($supported) <= 1) {
            return '';
        }

        $locales = [];
        foreach ($supported as $locale) {
            $locales[$locale] = Locale::getDisplayLanguage($locale, $locale);
        }
        $detected = $this->translationLocale();

        $view = $this->getView();
        return $view->partial(
            'common/multilingual-locale-selector',
            [
                'detected' => $detected,
                'locales' => $locales,
            ]
        );
    }
}
