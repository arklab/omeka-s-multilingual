<?php

namespace Multilingual\Mvc;

use Laminas\EventManager\AbstractListenerAggregate;
use Laminas\EventManager\EventManagerInterface;
use Laminas\Mvc\MvcEvent;
use Laminas\Session\Container;

class MvcListeners extends AbstractListenerAggregate
{
    /**
     * Attach listeners to MVC events.
     *
     * @param EventManagerInterface $events
     */
    public function attach(EventManagerInterface $events, $priority = 1): void
    {
        // Overrides translation init in Omeka\Mvc\MvcListeners::preparePublicSite()
        $this->listeners[] = $events->attach(
            MvcEvent::EVENT_ROUTE,
            [$this, 'preparePublicSite']
        );
    }

    /**
     * Overrides translation init in Omeka\Mvc\MvcListeners::preparePublicSite()
     *
     * @param MvcEvent $event
     */
    public function preparePublicSite(MvcEvent $event): void
    {
        // Only apply Multilingual to public sites
        $routeMatch = $event->getRouteMatch();
        if (!$routeMatch->getParam('__SITE__')) {
            return;
        }

        $services = $event->getApplication()->getServiceManager();
        $multilingual = $services->get('Multilingual');

        // HACK: Detect if any locale set in query, if yes then save in session and remove from query.
        // This should probably be being set in a cookie and loaded into the session from there
        $request = $event->getRequest();
        $query = $request->getQuery();
        $requested = null;
        if ($query->get('lang', null)) {
            $requested = $query->get('lang');
            $query->set('lang', null);
        }
        if ($query->get('locale', null)) {
            $requested = $query->get('locale');
            $query->set('locale', null);
        }
        if ($query->get('_lang', null)) {
            $requested = $query->get('_lang');
            $query->set('_lang', null);
        }
        if ($query->get('_locale', null)) {
            $requested = $query->get('_locale');
            $query->set('_locale', null);
        }
        if ($requested) {
            $storage = Container::getDefaultManager()->getStorage();
            if ($requested === 'auto') {
                $storage->offsetUnset('_locale');
            } else {
                $storage->offsetSet('_locale', $requested);
            }
            $request->setQuery($query);
        }

        $multilingual->detectLocale($request);
    }
}
