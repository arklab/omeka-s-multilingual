<?php

namespace Multilingual\Service;

use DirectoryIterator;
use Interop\Container\ContainerInterface;
use Laminas\Session\Container;
use Laminas\Stdlib\RequestInterface as Request;
use Locale;

class MultilingualService
{
    const GLOBAL_DEFAULT = 'en_US';

    protected $services;
    protected $themeLocales = null;

    public function __construct(ContainerInterface $services)
    {
        $this->services = $services;
    }

    // Detect and set the locale to use
    public function detectLocale(?Request $request = null) : void
    {
        $locale = $this->detectedLocale($request);
        $translation = $this->translationLocale($locale);
        $fallback = $this->fallbackLocale();
        $this->setLocale($locale, $translation, $fallback);
    }

    // Detect what primary locale to use. This locale will be the first one that matches a supported translation
    // locale, although the translation locale may differ, e.g. pt_BR as locale, pt as translation locale
    public function detectedLocale(?Request $request = null) : ?string
    {
        $locale = null;

        // The Site supported locales
        $supported = $this->supportedLocales();

        // First use any Session override Locale, i.e. set by the Locale Selector
        $session = $this->sessionLocale();
        if ($session && Locale::lookup($supported, $session)) {
            $locale = $session;
        }

        // Next use any logged in user preference
        if (!$locale) {
            $user = $this->userLocale();
            if (Locale::lookup($supported, $user)) {
                $locale = $user;
            }
        }

        // Next use any User Agent preferred locale that matches
        if (!$locale) {
            $preferred = $this->userAgentLocales($request);
            foreach ($preferred as $pref) {
                if (Locale::lookup($supported, $pref)) {
                    $locale = $pref;
                    break;
                }
            }
        }

        // Next use the default Site locale
        if (!$locale) {
            $locale = $this->defaultLocale();
        }

        return $locale;
    }

    public function translationLocale(string $locale) : ?string
    {
        $supported = $this->supportedLocales();
        return Locale::lookup($supported, $locale);
    }

    public function fallbackLocale() : ?string
    {
        $default = $this->defaultLocale();
        return $this->translationLocale($default);
    }

    public function sessionLocale() : ?string
    {
        $locale = null;
        $storage = Container::getDefaultManager()->getStorage();
        if ($storage->offsetExists('_locale')) {
            $locale = $storage->offsetGet('_locale') ?: null;
        }
        return $locale;
    }

    public function userLocale() : ?string
    {
        $locale = null;
        $auth = $this->services->get('Omeka\AuthenticationService');
        if ($auth->hasIdentity()) {
            $userId = $auth->getIdentity()->getId();
            $this->services->get('Omeka\Settings\User')->setTargetId($userId);
            $locale = $this->services->get('Omeka\Settings\User')->get('locale', null);
        }
        return $locale;
    }

    public function userAgentLocales(?Request $request = null) : array
    {
        $accept = [];
        if (!$request) {
            $request = $this->services->get('Request');
        }
        if ($request) {
            $headers = $request->getHeaders();
            if ($headers && $headers->has('Accept-Language')) {
                $prioritized = $headers->get('Accept-Language')->getPrioritized();
                foreach ($prioritized as $part) {
                    $bcp = $part->getLanguage();
                    $parts = explode('-', $bcp);
                    $accept[] = implode('_', $parts);
                }
            }
        }
        return $accept;
    }

    public function defaultLocale() : ?string
    {
        return $this->siteLocale() ?: $this->systemLocale() ?: $this->configLocale() ?: self::GLOBAL_DEFAULT;
    }

    public function siteLocale() : ?string
    {
        return $this->services->get('Omeka\Settings\Site')->get('locale', null);
    }

    public function systemLocale() : ?string
    {
        return $this->services->get('Omeka\Settings')->get('locale', null);
    }

    public function configLocale() : ?string
    {
        $config = $this->services->get('Config');
        return $config['translator']['locale'] ?: null;
    }

    public function supportedLocales() : array
    {
        return $this->siteSupportedLocales() ?: $this->systemSupportedLocales() ?: $this->themeSupportedLocales() ?: [$this->defaultLocale()];
    }

    public function siteSupportedLocales() : array
    {
        return $this->services->get('Omeka\Settings\Site')->get('multilingual_locales', []);
    }

    public function systemSupportedLocales() : array
    {
        return $this->services->get('Omeka\Settings')->get('multilingual_locales', []);
    }

    public function themeSupportedLocales() : array
    {
        if ($this->themeLocales === null) {
            $this->themeLocales = [];
            $themeManager = $this->services->get('Omeka\Site\ThemeManager');
            $currentTheme = $themeManager->getCurrentTheme();
            $languagePath = $currentTheme->getPath('language');
            $dir = new DirectoryIterator($languagePath);
            foreach ($dir as $fileinfo) {
                if ($fileinfo->getExtension() === 'mo') {
                    $this->themeLocales[] = $fileinfo->getBasename('.mo');
                }
            }
        }

        return $this->themeLocales;
    }

    public function setLocale(string $locale, ?string $translation = null, ?string $fallback = null) : void
    {
        if ($locale) {
            if (extension_loaded('intl') && $locale !== 'debug') {
                Locale::setDefault($locale);
            }
            $translation = $translation ?: $locale;
            $translator = $this->services->get('MvcTranslator');
            $translator->getDelegatedTranslator()->setLocale($translation);
            if ($fallback && $fallback !== $translation) {
                $translator->getDelegatedTranslator()->setFallbackLocale($fallback);
            }
        }
    }
}
