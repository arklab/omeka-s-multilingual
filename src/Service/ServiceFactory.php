<?php
namespace Multilingual\Service;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
* Create the Multilingual service.
*
* @return MultilingualService
 */
class ServiceFactory implements FactoryInterface
{
    /**
     * Create and return the Multilingual service.
     *
     * @return MultilingualService
     */
    public function __invoke(ContainerInterface $services, $requestedName, array $options = null)
    {
        return new MultilingualService($services);
    }
}
