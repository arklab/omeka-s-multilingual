<?php

namespace Multilingual\Service\ViewHelper;

use Multilingual\View\Helper\MultilingualHelper;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 * Service factory to get the Multilingual view helper.
 */
class ViewHelperFactory implements FactoryInterface
{
    /**
     * Create and return the MultilingualFactory view helper.
     *
     * @return MultilingualHelper
     */
    public function __invoke(ContainerInterface $services, $requestedName, array $options = null)
    {
        return new MultilingualHelper($services->get('Multilingual'));
    }
}
