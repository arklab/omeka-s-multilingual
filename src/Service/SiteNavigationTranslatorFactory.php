<?php
namespace Multilingual\Service;

use Interop\Container\ContainerInterface;
use Multilingual\Site\Navigation\SiteNavigationTranslator;
use Laminas\ServiceManager\Factory\FactoryInterface;

class SiteNavigationTranslatorFactory implements FactoryInterface
{
    /**
     * Create the Site\Navigation\SiteNavigationTranslator service.
     *
     * @return SiteNavigationTranslator
     */
    public function __invoke(ContainerInterface $serviceLocator, $requestedName, array $options = null)
    {
        return new SiteNavigationTranslator(
            $serviceLocator->get('Omeka\Site\NavigationLinkManager'),
            $serviceLocator->get('MvcTranslator'),
            $serviceLocator->get('ViewHelperManager')->get('Url')
        );
    }
}
