<?php
namespace Multilingual\Service\BlockLayout;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Multilingual\Site\BlockLayout\MultilingualHeading;
use Multilingual\Site\BlockLayout\MultilingualHtml;
use Multilingual\Site\BlockLayout\MultilingualKeyword;
use Multilingual\Site\BlockLayout\MultilingualSection;
use Multilingual\Site\BlockLayout\MultilingualSectionHtml;
use Multilingual\Site\BlockLayout\MultilingualSectionText;
use Multilingual\Site\BlockLayout\MultilingualText;


class BlockLayoutFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $services, $requestedName, array $options = null)
    {
        switch ($requestedName) {
            case 'multilingualHeading':
                return new MultilingualHeading($services);
            case 'multilingualHtml':
                return new MultilingualHtml($services);
            case 'multilingualKeyword':
                return new MultilingualKeyword($services);
            case 'multilingualSection':
                return new MultilingualSection($services);
            case 'multilingualSectionHtml':
                return new MultilingualSectionHtml($services);
            case 'multilingualSectionText':
                return new MultilingualSectionText($services);
            case 'multilingualText':
                return new MultilingualText($services);
        }
    }
}
