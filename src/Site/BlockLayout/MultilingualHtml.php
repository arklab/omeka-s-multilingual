<?php
namespace Multilingual\Site\BlockLayout;

use Multilingual\Form\HtmlFieldset;

class MultilingualHtml extends AbstractBlockLayout
{
    const BLOCK_TEMPLATE = 'common/block-layout/multilingual-html';
    const BLOCK_FIELDSETS = [HtmlFieldset::class];

    public function getLabel()
    {
        return 'Multilingual HTML'; // @translate
    }
}
