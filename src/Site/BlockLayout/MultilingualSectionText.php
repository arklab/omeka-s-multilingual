<?php

namespace Multilingual\Site\BlockLayout;

use Multilingual\Form\HeadingFieldset;
use Multilingual\Form\TextFieldset;

class MultilingualSectionText extends AbstractBlockLayout
{
    const BLOCK_TEMPLATE = 'common/block-layout/multilingual-section-html';
    const BLOCK_FIELDSETS = [HeadingFieldset::class, TextFieldset::class];

    public function getLabel()
    {
        return 'Multilingual Text Section'; // @translate
    }
}
