<?php
namespace Multilingual\Site\BlockLayout;

use Multilingual\Form\TextFieldset;

class MultilingualText extends AbstractBlockLayout
{
    const BLOCK_TEMPLATE = 'common/block-layout/multilingual-text';
    const BLOCK_FIELDSETS = [TextFieldset::class];

    public function getLabel()
    {
        return 'Multilingual Text'; // @translate
    }
}
