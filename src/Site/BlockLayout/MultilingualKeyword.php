<?php
namespace Multilingual\Site\BlockLayout;

use Multilingual\Form\KeywordFieldset;

class MultilingualKeyword extends AbstractBlockLayout
{
    const BLOCK_TEMPLATE = 'common/block-layout/multilingual-keyword';
    const BLOCK_FIELDSETS = [KeywordFieldset::class];

    public function getLabel()
    {
        return 'Multilingual Keyword'; // @translate
    }
}
