<?php

namespace Multilingual\Site\BlockLayout;

use Interop\Container\ContainerInterface;
use Laminas\View\Renderer\PhpRenderer;
use Multilingual\Form\AbstractBlockFieldset;
use Omeka\Api\Representation\SiteRepresentation;
use Omeka\Api\Representation\SitePageRepresentation;
use Omeka\Api\Representation\SitePageBlockRepresentation;
use Omeka\Api\Representation\AbstractRepresentation;
use Omeka\Site\BlockLayout\AbstractBlockLayout as OmekaAbstractBlockLayout;
use Omeka\Site\BlockLayout\TemplateableBlockLayoutInterface;
use Omeka\Entity\SitePageBlock;
use Omeka\Stdlib\ErrorStore;

abstract class AbstractBlockLayout extends OmekaAbstractBlockLayout implements TemplateableBlockLayoutInterface
{
    const BLOCK_TEMPLATE = '';
    const BLOCK_FIELDSETS = [];

    protected $services;

    public function __construct(ContainerInterface $services){
        $this->services = $services;
    }

    public function populateFieldsetValues(AbstractBlockFieldset $fieldset, ?SitePageBlockRepresentation $block = null)
    {
        $purifier = $this->services->get('Omeka\HtmlPurifier');
        $data = $block ? $block->data() : [];
        $data = $fieldset->normaliseBlockData($data, $purifier);
        $formData = [];
        foreach ($data as $key => $value) {
            $formData["o:block[__blockIndex__][o:data][$key]"] = $value;
        }
        $fieldset->populateValues($formData);
    }

    public function getFieldset(string $fieldset) {
        $formElementManager = $this->services->get('FormElementManager');
        return $formElementManager->get($fieldset);
    }

    public function onHydrate(SitePageBlock $block, ErrorStore $errorStore)
    {
        $purifier = $this->services->get('Omeka\HtmlPurifier');
        $data = $block->getData();
        foreach (static::BLOCK_FIELDSETS as $key) {
            $fieldset = $this->getFieldset($key);
            $data = $fieldset->normaliseBlockData($data, $purifier);
        }
        $block->setData($data);
    }

    public function form(
        PhpRenderer $view,
        SiteRepresentation $site,
        SitePageRepresentation $page = null,
        SitePageBlockRepresentation $block = null
    ) {
        $html = '';
        foreach (static::BLOCK_FIELDSETS as $key) {
            $fieldset = $this->getFieldset($key);
            $this->populateFieldsetValues($fieldset, $block);
            $html .= $view->formCollection($fieldset, false);
        }
        return $html;
    }

    public function render(PhpRenderer $view, SitePageBlockRepresentation $block, $templateViewScript = null)
    {
        $data = $block->data();
        $values = ['block' => $block];
        foreach (static::BLOCK_FIELDSETS as $key) {
            $fieldset = $this->getFieldset($key);
            $values += $fieldset->viewDataValues($view, $data);
        }
        return $view->partial($templateViewScript ?? static::BLOCK_TEMPLATE, $values);
    }

    public function getFulltextText(PhpRenderer $view, SitePageBlockRepresentation $block)
    {
        return strip_tags($this->render($view, $block));
    }
}
