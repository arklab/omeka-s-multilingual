<?php

namespace Multilingual\Site\BlockLayout;

use Multilingual\Form\HeadingFieldset;

class MultilingualHeading extends AbstractBlockLayout
{
    const BLOCK_TEMPLATE = 'common/block-layout/multilingual-heading';
    const BLOCK_FIELDSETS = [HeadingFieldset::class];

    public function getLabel()
    {
        return 'Multilingual Heading'; // @translate
    }
}
