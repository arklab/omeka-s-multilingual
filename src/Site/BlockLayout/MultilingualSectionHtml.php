<?php

namespace Multilingual\Site\BlockLayout;

use Multilingual\Form\HeadingFieldset;
use Multilingual\Form\HtmlFieldset;

class MultilingualSectionHtml extends AbstractBlockLayout
{
    const BLOCK_TEMPLATE = 'common/block-layout/multilingual-section-html';
    const BLOCK_FIELDSETS = [HeadingFieldset::class, HtmlFieldset::class];

    public function getLabel()
    {
        return 'Multilingual HTML Section'; // @translate
    }
}
