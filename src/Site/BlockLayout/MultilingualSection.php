<?php

namespace Multilingual\Site\BlockLayout;

use Multilingual\Form\HeadingFieldset;
use Multilingual\Form\KeywordFieldset;

class MultilingualSection extends AbstractBlockLayout
{
    const BLOCK_TEMPLATE = 'common/block-layout/multilingual-section';
    const BLOCK_FIELDSETS = [HeadingFieldset::class, KeywordFieldset::class];

    public function getLabel()
    {
        return 'Multilingual Section'; // @translate
    }
}
