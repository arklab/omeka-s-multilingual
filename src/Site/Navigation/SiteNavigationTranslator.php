<?php
namespace Multilingual\Site\Navigation;

use Omeka\Site\Navigation\Translator;
use Omeka\Api\Representation\SiteRepresentation;
use Omeka\Site\Navigation\Link\LinkInterface;

class SiteNavigationTranslator extends Translator
{
    /**
     * Override the default which doesn't translate labels
     */
    public function getLinkLabel(LinkInterface $linkType, array $data, SiteRepresentation $site)
    {
        $label = $linkType->getLabel($data, $site) ?: $linkType->getName();
        return $this->i18n->translate($label);
    }
}
