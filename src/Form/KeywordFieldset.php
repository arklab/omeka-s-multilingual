<?php

namespace Multilingual\Form;

use Laminas\Form\Element;

class KeywordFieldset extends AbstractBlockFieldset
{
    public function config(): array
    {
        return [
            'keyword' => [
                'type' => Element\Text::class,
                'options' => [
                    'label' => 'Keyword', // @translate
                ],
                'default' => '',
            ],
            'domain' => [
                'type' => Element\Text::class,
                'options' => [
                    'label' => 'Domain', // @translate
                ],
                'default' => null,
            ],
            'locale' => [
                'type' => Element\Text::class,
                'options' => [
                    'label' => 'Locale', // @translate
                ],
                'default' => null,
            ],
        ];
    }
}
