<?php

// Modified from Omeka\Site\BlockLayout\Html

namespace Multilingual\Form;

use Laminas\Form\Element;

class HtmlFieldset extends AbstractBlockFieldset
{
    public function config(): array
    {
        return [
            'html' => [
                'type' => Element\Textarea::class,
                'attributes' => [
                    'class' => 'block-html full wysiwyg',
                ],
                'default' => '',
            ],
        ];
    }

    public function normaliseBlockData(array $data, $purifier = null): array
    {
        $data = $data + $this->defaultBlockData();
        $html = trim((string) $data['html']);
        $data['html'] = $purifier->purify($html);
        return $data;
    }
}
