<?php

namespace Multilingual\Form;

use Laminas\Form\Fieldset;
use Omeka\Form\Element\ArrayTextarea;

class SettingsFieldset extends Fieldset
{
    protected $label = 'Multilingual'; // @translate

    protected $elementGroups = [
        'multilingual' => 'Multilingual', // @translate
    ];

    public function init(): void
    {
        $this->setAttribute('id', 'multilingual')
            ->setOption('element_groups', $this->elementGroups)
            ->add(
                [
                    'name' => 'multilingual_locales',
                    'type' => ArrayTextarea::class,
                    'options' => [
                        'element_group' => 'multilingual',
                        'label' => 'Locales', // @translate
                        'info' => 'List all supported locales, one per line.', // @translate
                    ],
                    'attributes' => [
                        'id' => 'multilingual_locales',
                        'placeholder' => 'en_US',
                    ],
                ]
            );
    }
}
