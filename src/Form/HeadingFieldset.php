<?php

// Modified from BlockPlus\Form\HeadingFieldset

namespace Multilingual\Form;

use Common\Form\Element as CommonElement;
use Laminas\Form\Element;
use Laminas\View\Renderer\PhpRenderer;

class HeadingFieldset extends AbstractBlockFieldset
{
    public function config(): array
    {
        return [
            'heading' => [
                'type' => Element\Text::class,
                'options' => [
                    'label' => 'Heading', // @translate
                ],
                'default' => '',
            ],
            'level' => [
                'type' => CommonElement\OptionalRadio::class,
                'options' => [
                    'label' => 'Level', // @translate
                    'value_options' => [
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '5' => '5',
                        '6' => '6',
                    ],
                ],
                'attributes' => [
                    'value' => '3',
                ],
                'default' => '3',
            ],
            'page' => [
                'type' => CommonElement\OptionalSitePageSelect::class,
                'options' => [
                    'label' => 'Page', // @translate
                    'empty_option' => '',
                ],
                'default' => null,
            ],
            'url' => [
                'type' => CommonElement\OptionalUrl::class,
                'options' => [
                    'label' => 'URL', // @translate
                ],
                'default' => null,
            ],
        ];
    }

    public function normaliseBlockData(array $data, $purifier = null): array
    {
        $data = $data + $this->defaultBlockData();
        $data['heading'] = trim((string) $data['heading'] ?? '');
        $level = (int) $data['level'];
        $data['level'] = (string) ($level < 1 || $level > 6 ? 3 : $level);
        if ($data['page'] === '') {
            $data['page'] = null;
        }
        return $data;
    }

    public function viewDataValues(PhpRenderer $view, array $data): array
    {
        $values = [];
        $values['heading'] = $data['heading'];
        $values['level'] = $data['level'];
        if ($data['page']) {
            $page = $view->api()->read('site_pages', $data['page'])->getContent();
            if ($page) {
                $values['url'] = $page->siteUrl();
            }
        } else {
            $values['url'] = $data['url'];
        }
        $values['domain'] = null;
        $values['locale'] = null;
        return $values;
    }
}
