<?php

namespace Multilingual\Form;

use Laminas\Form\Element;

class TextFieldset extends AbstractBlockFieldset
{
    public function config(): array
    {
        return [
            'text' => [
                'type' => Element\Textarea::class,
                'options' => [
                    'label' => 'Text', // @translate
                ],
                'default' => '',
            ],
            'domain' => [
                'type' => Element\Text::class,
                'options' => [
                    'label' => 'Domain', // @translate
                ],
                'default' => null,
            ],
            'locale' => [
                'type' => Element\Text::class,
                'options' => [
                    'label' => 'Locale', // @translate
                ],
                'default' => null,
            ],
        ];
    }
}
