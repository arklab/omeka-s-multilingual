<?php

namespace Multilingual\Form;

use Laminas\Form\Fieldset;
use Laminas\View\Renderer\PhpRenderer;

abstract class AbstractBlockFieldset extends Fieldset
{
    protected array $fieldNames = [];
    protected array $defaultData = [];
    protected array $defaultBlockData = [];

    abstract public function config(): array;

    public function init(): void
    {
        foreach ($this->config() as $key => $config) {
            $name = "o:block[__blockIndex__][o:data][$key]";
            $config['name'] = $name;
            $this->fieldNames[$key] = $name;
            if (array_key_exists('default', $config)) {
                $this->defaultData[$name] = $config['default'];
                $this->defaultBlockData[$key] = $config['default'];
                unset($config['default']);
            }
            $this->add($config);
        }
    }

    public function blockFieldNames(): array
    {
        return array_keys($this->fieldNames);
    }

    public function fieldNames(): array
    {
        return array_values($this->fieldNames);
    }

    public function defaultBlockData(): array
    {
        return $this->defaultBlockData;
    }

    public function defaultData(): array
    {
        return $this->defaultData;
    }

    public function normaliseBlockData(array $data, $purifier = null): array
    {
        $data = $data + $this->defaultBlockData();
        foreach ($data as $key => $value) {
            if ($value === '')  {
                $data[$key] = $this->defaultBlockData[$key];
            }
        }
        return $data;
    }

    public function viewDataValues(PhpRenderer $view, array $data): array
    {
        foreach ($data as $key => $value) {
            if ($value === '')  {
                $data[$key] = $this->defaultBlockData[$key];
            }
        }
        return $data;
    }
}
