$(document).ready(function () {
    $('#multilingual-locale-select').on('change', function () {
        let locale = $(this).children("option:selected").val();
        let params = new URLSearchParams(document.location.search);
        params.delete('lang');
        params.delete('_lang');
        params.delete('locale');
        params.set('_locale', locale);
        query = params.toString();
        if (query == '?') {
            query = '';
        }
        document.location.search = query;
    });
});
