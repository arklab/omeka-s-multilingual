# Multilingual - A Module for Omeka S

Multilingual is an Omeka S module to support multilingual Sites. The module provides tools to auto-detect or select the
required locale, and blocks and templates to display content in that locale.

Multilingual determines the locale based on the browser user agent settings and the supported locales for the site or
system, selecting the first supported user language, or falling back to the default Site locale. Users can choose to
switch to any of the supported locales.

Currently, a minimally modified theme or module is required to provide the translation files, but future plans may remove
this requirement.

## Installation

This Module only supports Omeka S v4 or higher.

This Module requires the Omeka S [Common](https://gitlab.com/Daniel-KM/Omeka-S-module-Common) module to be installed.

A minimally modified theme (or module) is required to provide the translation files for the Site. The simplest option is
to modify the Default Theme `config/theme.ini` to set the translations flag as below and save the translation files in
the `language` subfolder:

```
has_translations = "true"
```

See Locale Selector below for other optional modifications to the theme.

## Settings

You can set the list of supported locales for each Site individually, or at System level, or the list will default to
the locale files available in your Theme.

The fallback locale for translations will be either the Site, System or Config default locale.

## View Helper

A view helper is provided which can be accessed in any view by calling `$this->multilingual()`.

## Locale Selector

A locale selector widget is provided to allow users to switch the detected locale to another supported locale.
To use this you will need to edit your Theme `view\layout\layout.phtml` file.

To add the required js and css call:

```
$this->multilingual()->prepareView($this);
```

To display the selector call:

```
echo $this->multilingual()->localeSelector();
```

## Blocks

Multilingual provides a set of Blocks that translate their displayed content into the detected locale:

* Multilingual Heading
* Multilingual HTML
* Multilingual Keyword
* Multilingual Text
* Multilingual Section

Multilingual Heading allows you to set the heading text or keyword, the heading level, and an optional heading link.

The Multilingual Section block has a number of versions that combine a Heading with HTML, Text, or a Keyword.

Contributions or suggestions of other blocks are welcome.

## Block Layouts

Some core Omeka S blocks and other module blocks do not translate content, such as the core HTML block.
Multilingual provides Block Layout templates that do translate content to use instead of the default templates:

* HTML
* BlockPlus Heading
* BlockPlus Heading Link

To use these these templates, select the Configure Layout for the Block and set the Template to a Multilingual option.

Contributions or suggestions of other blocks to support are welcome.

## Future plans

Short term we plan to add more Blocks and Block Layouts.

Long term we may add a built-in translation management system, with translations stored in database tables and edited
in the Admin site. This will allow non-coders to maintain the translations and use them in their pages without the need
to generate translation files and maintain them in a theme or module.
