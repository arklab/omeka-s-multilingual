<?php

namespace Multilingual;

if (!class_exists(\Common\TraitModule::class)) {
    require_once dirname(__DIR__) . '/Common/TraitModule.php';
}

use Common\TraitModule;
use Laminas\EventManager\SharedEventManagerInterface;
use Omeka\Module\AbstractModule;

class Module extends AbstractModule
{
    const NAMESPACE = __NAMESPACE__;

    use TraitModule;

    public function attachListeners(SharedEventManagerInterface $sharedEventManager): void
    {
        // System Settings.
        $sharedEventManager->attach(
            \Omeka\Form\SettingForm::class,
            'form.add_elements',
            [$this, 'handleMainSettings']
        );
        // Site Settings.
        $sharedEventManager->attach(
            \Omeka\Form\SiteSettingsForm::class,
            'form.add_elements',
            [$this, 'handleSiteSettings']
        );
        // Copy Resources support.
        $sharedEventManager->attach(
            '*',
            'copy_resources.sites.post',
            function (Event $event) {
                $copyResources = $event->getParam('copy_resources');
                $siteCopy = $event->getParam('resource_copy');
                $copyResources->revertSiteBlockLayouts($siteCopy->id(), 'multilingualHeading');
                $copyResources->revertSiteBlockLayouts($siteCopy->id(), 'multilingualHtml');
                $copyResources->revertSiteBlockLayouts($siteCopy->id(), 'multilingualKeyword');
                $copyResources->revertSiteBlockLayouts($siteCopy->id(), 'multilingualSection');
                $copyResources->revertSiteBlockLayouts($siteCopy->id(), 'multilingualSectionHtml');
                $copyResources->revertSiteBlockLayouts($siteCopy->id(), 'multilingualSectionText');
                $copyResources->revertSiteBlockLayouts($siteCopy->id(), 'multilingualText');
            }
        );
    }

}
