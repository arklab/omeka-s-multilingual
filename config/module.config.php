<?php
namespace Multilingual;

return [
    'form_elements' => [
        'invokables' => [
            Form\HeadingFieldset::class => Form\HeadingFieldset::class,
            Form\HtmlFieldset::class => Form\HtmlFieldset::class,
            Form\KeywordFieldset::class => Form\KeywordFieldset::class,
            Form\SettingsFieldset::class => Form\SettingsFieldset::class,
            Form\SiteSettingsFieldset::class => Form\SiteSettingsFieldset::class,
            Form\TextFieldset::class => Form\TextFieldset::class,
        ],
    ],
    'listeners' => [
        Mvc\MvcListeners::class,
    ],
    'multilingual' => [
        'settings' => [
            'multilingual_locales' => [],
        ],
        'site_settings' => [
            'multilingual_locales' => [],
        ],
        'block_settings' => [
            'multilingualHeading' => [
                'heading' => '',
                'level' => '3',
                'page' => '',
                'url' => '',
            ],
            'multilingualHtml' => [
                'html' => '',
            ],
            'multilingualKeyword' => [
                'keyword' => '',
                'domain' => '',
                'locale' => '',
            ],
            'multilingualSection' => [
                'heading' => '',
                'level' => '3',
                'page' => '',
                'url' => '',
                'keyword' => '',
                'domain' => '',
                'locale' => '',
            ],
            'multilingualSectionHtml' => [
                'heading' => '',
                'level' => '3',
                'page' => '',
                'url' => '',
                'html' => '',
            ],
            'multilingualSectionText' => [
                'heading' => '',
                'level' => '3',
                'page' => '',
                'url' => '',
                'text' => '',
                'domain' => '',
                'locale' => '',
            ],
            'multilingualText' => [
                'text' => '',
                'domain' => '',
                'locale' => '',
            ],
        ],
    ],
    'service_manager' => [
        'invokables' => [
            Mvc\MvcListeners::class => Mvc\MvcListeners::class,
         ],
        'factories' => [
            'Multilingual' => Service\ServiceFactory::class,
            'Omeka\Site\NavigationTranslator' => Service\SiteNavigationTranslatorFactory::class,
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            dirname(__DIR__).'/view',
        ],
    ],
    'view_helpers' => [
        'factories' => [
            'multilingual' => Service\ViewHelper\ViewHelperFactory::class,
        ],
    ],
    'block_layouts' => [
        'factories' => [
            'multilingualHeading' => Service\BlockLayout\BlockLayoutFactory::class,
            'multilingualHtml' => Service\BlockLayout\BlockLayoutFactory::class,
            'multilingualKeyword' => Service\BlockLayout\BlockLayoutFactory::class,
            'multilingualSection' => Service\BlockLayout\BlockLayoutFactory::class,
            'multilingualSectionHtml' => Service\BlockLayout\BlockLayoutFactory::class,
            'multilingualSectionText' => Service\BlockLayout\BlockLayoutFactory::class,
            'multilingualText' => Service\BlockLayout\BlockLayoutFactory::class,
        ],
    ],
    'block_templates' => [
        'heading' => [
            'multilingual-blockplus-heading' => 'Multilingual: Heading', // @translate
            'multilingual-blockplus-heading-link' => 'Multilingual: Heading link', // @translate
        ],
        'html' => [
            'multilingual-omekas-html' => 'Multilingual: HTML', // @translate
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => dirname(__DIR__).'/language',
                'pattern' => '%s.mo',
                'text_domain' => null,
            ],
        ],
    ],
];
